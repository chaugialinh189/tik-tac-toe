﻿string[,] arr = new string[3, 3];
String player1 = "X";
String player2 = "O";
int x1, y1, x2, y2;
int count = 0;
Console.WriteLine("Quy uoc: ");
Console.WriteLine("Player 1: X");
Console.WriteLine("Player 2: O");
do
{
    Console.WriteLine("Luot cua nguoi choi 1 (Nhap toa do x va y): ");
    do
    {
        x1 = int.Parse(Console.ReadLine());
        y1 = int.Parse(Console.ReadLine());
        if (arr[x1 - 1, y1 - 1] != null)
        {
            Console.WriteLine("Da co nguoi choi danh cho nay");
        }
    } while (arr[x1 - 1, y1 - 1] != null);
    count++;// dem so lan danh de hoa
    arr[x1 - 1, y1 - 1] = player1;
    Console.WriteLine(" ----- ----- -----");
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (arr[i, j] != null)
            {
                Console.Write($"|  {arr[i, j]}  ");
            }
            else
            {
                Console.Write("|     ");
            }
        }
        Console.Write("|");
        Console.WriteLine();
        Console.WriteLine(" ----- ----- -----");
    }
    if ((arr[0, 0] == player1 && arr[1, 1] == player1 && arr[2, 2] == player1) || (arr[0, 2] == player1 && arr[1, 1] == player1 && arr[2, 0] == player1)
        || (arr[0, 0] == player1 && arr[1, 0] == player1 && arr[2, 0] == player1) || (arr[0, 1] == player1 && arr[1, 1] == player1 && arr[2, 1] == player1)
        || (arr[0, 2] == player1 && arr[1, 2] == player1 && arr[2, 2] == player1) || (arr[0, 0] == player1 && arr[0, 1] == player1 && arr[0, 2] == player1)
        || (arr[1, 0] == player1 && arr[1, 1] == player1 && arr[1, 2] == player1) || (arr[2, 0] == player1 && arr[2, 1] == player1 && arr[2, 2] == player1))
    {
        Console.WriteLine("Player 1 win");
        break;
    }
    if (count == 9)
    {
        Console.WriteLine("Hai nguoi choi hoa");
        break;
    }
    Console.WriteLine("Luot cua nguoi choi thu 2 (Nhap toa do x va y): ");
    do
    {
        x2 = int.Parse(Console.ReadLine());
        y2 = int.Parse(Console.ReadLine());
        if (arr[x2 - 1, y2 - 1] != null)
        {
            Console.WriteLine("Da co nguoi choi danh cho nay");
        }
    } while (arr[x2 - 1, y2 - 1] != null);
    count++;
    arr[x2 - 1, y2 - 1] = player2;
    Console.WriteLine(" ----- ----- -----");
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (arr[i, j] != null)
            {
                Console.Write($"|  {arr[i, j]}  ");
            }
            else
            {
                Console.Write("|     ");
            }
        }
        Console.Write("|");
        Console.WriteLine();
        Console.WriteLine(" ----- ----- -----");
    }
    if ((arr[0, 0] == player2 && arr[1, 1] == player2 && arr[2, 2] == player2) || (arr[0, 2] == player2 && arr[1, 1] == player2 && arr[2, 0] == player2)
        || (arr[0, 0] == player2 && arr[1, 0] == player2 && arr[2, 0] == player2) || (arr[0, 1] == player2 && arr[1, 1] == player2 && arr[2, 1] == player2)
        || (arr[0, 2] == player2 && arr[1, 2] == player1 && arr[2, 2] == player2) || (arr[0, 0] == player1 && arr[0, 1] == player2 && arr[0, 2] == player2)
        || (arr[1, 0] == player1 && arr[1, 1] == player1 && arr[1, 2] == player2) || (arr[2, 0] == player2 && arr[2, 1] == player2 && arr[2, 2] == player2))
    {
        Console.WriteLine("Player 2 win");
        break;
    }
} while (count < 9);